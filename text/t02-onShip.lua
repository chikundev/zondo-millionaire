return {
    {
        type    = "fade",
        nextBG  = gfx.bg.greyShip
    },
    {
        type    = "text",
        text    = "You wake to the feeling of cold metal and darkness."
    },
    {
        type    = "text",
        text    = "Rubbing your head, you realize your memory of the circumstances " ..
            "as to how you arrived here is completely shot."
    },
    {
        type    = "text",
        text    = "Dread fills you."
    },
    {
        type    = "text",
        text    = "You have little time to deal with the circumstances of your " ..
            "predicament as a shaft of light punctures the darkness."
    },
    {
        type    = "text",
        text    = "An indistinct, hovering object floats through."
    },
    {
        type    = "fade",
        nextFG  = gfx.fg.murrayStandard
    },
    {
        type    = "text",
        text    = "It is an eye. A floating eye."
    },
    {
        type    = "text",
        speaker = "???",
        text    = "XXXX XXX? XZXX XXZX ZXXZ. ZZZZ!"
    },
    {
        type = "choice",
        text = "You are filled with a sense of complete and utter terror.",
        choices = {
            {
                text = "Argh!",
                nextStep = 11
            },
            {
                text = "Aiee!",
                nextStep = 11
            },
            {
                text = "AAAA!",
                nextStep = 11
            }
        }
    },
    {
        type = "choice",
        text = "You are frozen with fear as the eye moves closer and extends a " ..
            "proboscis-like appendage towards you. It seems to be holding something.",
        choices = {
            {
                text = "Attempt to run away",
                nextStep = 12
            },
            {
                text = "Attempt to resist the eye",
                nextStep = 15
            },
            {
                text = "Do nothing",
                nextStep = 19
            }
        }
    },
    -- RUN AWAY, 12
    {
        type    = "text",
        text    = "You desperately scramble away from the eye, only to hit a wall. " ..
            "As the eye continues its pursuit, you face another wall."
    },
    {
        type    = "text",
        text    = "And another."
    },
    {
        type = "text",
        text = "You come to the horrific realization that you are trapped, with the " ..
            "eye, in this room, and there is nothing you can do about it."
    },
    -- RESIST EYE, 15
    {
        type = "text",
        text = "With no other choice, you aim a punch at the eye, only for it to be " ..
            "swatted away effortlessly."
    },
    {
        type = "text",
        text = "You lash out again. Once more, the eye deflects your punch. No matter " ..
            "how hard you try, the eye cannot be defeated."
    },
    {
        type = "text",
        text = "The eye itself emits a louder and more frantic noise."
    },
    {
        type = "text",
        speaker = "???",
        text = "XZZZ! XZZZ! XZZZ!"
    },
    -- DO NOTHING, 19
    {
        type = "text",
        text = "fucking christ"
    }
}
