return {
    {
        type    = "text",
        text    = "October 3rd, 1998\nSome point along Route 80, Nevada"
    },
    {
        type    = "fade",
        nextBG = gfx.bg.nevadaRoute80
    },
    {
        type    = "text",
        text    = "Your car broke down, and the engine's dead.",
        code    = function()
            bgm.nevadaRoute80:play()
        end
    },
    {
        type    = "text",
        text    = "There's no power and the chill of the night air slowly seeps into your vehicle as " ..
            "the hours stretch on."
    },
    {
        type    = "text",
        text    = "It feels late, though the sun has long set, and the stars are the only mark of the " ..
            "indiscernible time."
    },
    {
        type    = "choice",
        text    = "You could go outside to see if you can flag a car down. Or stay inside.",
        choices = {
            {
                text = "Leave the car",
                nextStep = 9
            },
            {
                text = "Stay",
                nextStep = 7
            }
        }
    },
        {
            type = "text",
            text = "You stay inside."
        },
        {
            type = "text",
            text = "Unsuccessfully, you try to doze off. What feels like hours passes, but time may as " ..
                "well remain immobile.",
            nextStep = 12
        },
        {
            type = "text",
            text = "You decide to leave the car."
        },
        {
            type = "text",
            text = "The road is vague as it spans towards the contours that discern the desert floor from " ..
                "the night skies."
        },
        {
            type = "text",
            text = "There are no lights to mark the impossibly large scale of the surrounding terrain but " ..
                "the headlights of your sedan."
        },
    {
        type = "text",
        text = "Then, out of the dead silence comes a sudden and faint hum.",
        code    = function()
            bgm.nevadaRoute80:stop()
            bgm.dullHum:play()
        end
    },
    {
        type = "text",
        text = "At first, a distant noise, it becomes louder and louder, transforming " ..
            "into what you can only describe as a metallic scream."
    },
    {
        type = "text",
        text = "It is then that you see it. A blinding light.",
    },
    {
        type = "fade",
        nextBG = gfx.flash
    },
    {
        type = "fade",
        nextBG = gfx.bg.nevadaRoute80
    },
    {
        type = "fade",
        nextFG = gfx.fg.light
    },
    {
        type = "text",
        text = "A shining beacon darts across the skies and illuminates everything " ..
        "below it, a spotlight travelling quickly towards the direction of your car.",
    },
    {
        type = "text",
        speaker = "You",
        text = "What on Earth-",
    },
    {
        type = "choice",
        text = "The object comes ever closer. Almost caught in its headlights, you " ..
            "feel a sense of urgency overcome you.",
        choices = {
            {
                text = "Run away",
                nextStep = 20
            },
            {
                text = "Hide in the car",
                nextStep = 25
            },
            {
                text = "Move closer to the lights",
                nextStep = 29
            }
        }
    },
    -- Run away, 20
        {
            type = "text",
            text = "In a sense of panic, you begin to run into the darkness, hoping the " ..
                "behemoth of light does not follow you.",
        },
        {
            type = "text",
            text = "But it does, its lights sweeping over the car, reflecting directly " ..
                "towards you, exposing you to the object.",
        },
        {
            type = "text",
            text = "Desert sands underfoot, you can't help feeling like a wild animal, " ..
                "caught in the sight of a predator.",
        },
        {
            type = "text",
            text = "Step after step, you run, until you can run no more. It's as if the " ..
                "lights are playing a game with you.",
        },
        {
            type = "text",
            text = "Out of breath and energy, you give up, stopping for breath.",
            nextStep = 31
        },
    -- Turn off lights, 25
        {
            type = "text",
            text = "You turn to the car's controls and dim the interior lights.",
        },
        {
            type = "text",
            text = "You hope that you can not only conceal yourself from the quickly " ..
                "moving object, but get a better look at what it is.",
        },
        {
            type = "text",
            text = "But it is no use. The object continues to move in your direction, " ..
                "pointing its light towards the car.",
        },
        {
            type = "text",
            text = "You figure there is no choice but to abandon your vehicle and " ..
                "investigate the source of the object hovering towards you.",
        },
    -- Move closer to the lights, 29
        {
            type = "text",
            text = "You move closer to the lights, with only your hand to block their " ..
                "blinding intensity. You become the focus from this source of light."
        },
        {
            type = "text",
            text = "The cold of the night air dissapates as you walk ever closer, still " ..
                "unable to determine what it is that hovers above you."
        },
    {
        type = "text",
        text = "You stop as the object hovers directly above you, and you find " ..
            "yourself immobile. Every muscle in your body feels as if it has given way"
    },
    {
        type = "text",
        text = "Paralyzed with fear, overcome with the blaring noise of the object, " ..
            "and unable to move, you find yourself slowly losing consciousness. "
    },
    {
        type = "text",
        text = "The last thing you feel is a slow, but firm, force lift you higher " ..
            "and higher, towards the source of the lights."
    },
    {
        type = "fade",
        nextBG = gfx.flash,
        nextFG = "none"
    },
    {
        type = "text",
        text = "The heat from their rays intensifies rapidly."
    },
    {
        type = "text",
        text = "And then darkness.",
        setBG = "none",
        code    = function()
            bgm.dullHum:stop()
        end
    },
    {
        type    = "text",
        code    = function()
            dialogue.start("t02-onShip")
        end
    },


    --[[
    {
        type    = "quiz",
        text    = "Psar Alpha III is the home planet of which famed joob " ..
            "music composer, who recently became the posthumous recipient " ..
            "of the 46XXth PsArts Appreciation Award?",
        choices = {
            {
                text = "Con Moodle",
                nextStep = 3
            },
            {
                text = "Jondo \"Jizz\" Mondo",
                nextStep = 3
            },
            {
                text = "Og Wando Jr.",
                nextStep = 4
            },
            {
                text = "King Praeus the Meaty",
                nextStep = 3
            }
        },
        setBG   = "none",
        setFG   = "none"
    }]]--


}
