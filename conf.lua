-- chikun :: 2015
-- The configuration file for our tutorial


--[[
    The function which runs at the start of the game, and
    allows us to modify the game window
  ]]
function love.conf(game)

    game.version    = "0.10.0"

    game.window.width   = 854
    game.window.height  = 480
    game.window.minwidth = 800
    game.window.minheight = 450
    game.window.title   = "Zondo Millionaire"
    game.window.resizable = true

    --[[
        On Windows, will attach a debug console to the game.
        Set to false before distribution.
      ]]
    game.console = false

    game.window.fsaa = 4

end
