-- chikun :: 2014
-- Main menu state


-- Temporary state, removed at end of script
local menuState = { }


-- On state create
function menuState:create()

end


-- On state update
function menuState:update(dt)

end


-- On state draw
function menuState:draw()

end


-- On state kill
function menuState:kill()

end


-- Transfer data to state loading script
return menuState
