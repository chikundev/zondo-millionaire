-- chikun :: 2015
-- HUD state


-- Temporary state, removed at end of script
local hudState = { }


-- On state create
function hudState:create()

    scene = {
        bg = nil,
        fg = nil
    }

    dialogue.start("t01-intro")

end


-- On state update
function hudState:update(dt)

    dialogue.update(dt)

end


-- On state draw
function hudState:draw()

    dialogue.draw()

end


-- On state click
function hudState:click(x, y)

    dialogue.click(x, y)

end



-- On state kill
function hudState:kill()

end


-- Transfer data to state loading script
return hudState
