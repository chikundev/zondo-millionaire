-- chikun :: 2014
-- Template menu


-- Temporary menu, removed at end of script
local menu = { }


-- On menu create
function menu:create()

    menuButtons = {
        {
            text = "Resume",
            code = function()
                state.set(states.play)
            end
        },
        {
            text = "Save / Load",
            code = function()

            end
        },
        {
            text = "Options",
            code = function()

            end
        },
        {
            text = "Quit",
            code = function()
                e.quit()
            end
        }
    }

    menuFade = 0

    menuFadeDir = 1

    menuSelected = 0

end


-- On menu update
function menu:update(dt)

    menuFade = math.clamp(0, menuFade + dt * 4 * menuFadeDir, 1)

    if menuFade == 1 then

        menuSelected = 0

        for key, value in ipairs(menuButtons) do

            if math.overlap(cursor, {
                        x = 440,
                        y = 32 + (96 * key),
                        w = 400,
                        h = 48
                    }) then

                menuSelected = key

                nextCursor = "hand"

            end

        end

    elseif menuFade == 0 then

        menuButtons[menuSelected].code()

    end

end


function menu:click(x, y)

    if menuSelected > 0 and menuFade == 1 then

        menuFadeDir = -1

    end

end


-- On menu draw
function menu:draw()

    lastState:draw()

    screenFade = menuFade

    if menuFadeDir == -1 and menuSelected > 1 then

        screenFade = 1

    end

    g.setColor(0, 0, 0, 128 * screenFade)

    g.rectangle('fill', 0, 0, 1280, 720)

    local reverseFade = (1 - menuFade)

    for key, value in ipairs(menuButtons) do

        local x, y, w, h =
            440,
            32 + (key * 96),
            400,
            48

        g.setFont(fnt.smaller)

        -- Box fronts
        g.setColor(61, 79, 94)   -- Reset colour

        if key == menuSelected then
            g.setColor(100, 120, 140)
        end

        g.rectangle('fill', x , y + 800 * reverseFade, w, h)

        -- Box texts
        g.setColor(255, 255, 255)

        g.printf(value.text, x + 2, y + 13 + 800 * reverseFade, w - 4, 'center')

    end

end


-- On menu kill
function menu:kill()

end


-- Transfer data to state loading script
return menu
