function useKey(key)

    mouseControl = false

    local currentText = { }

    local atStopState = false

    if scene then

        -- Get current text segment
        currentText = scene.dialogue[scene.step]

        atStopState = ((currentText.type == "quiz" or currentText.type == "choice") and
            scene.textFadeStep == 1)

    end

    if state.current == states.play then

        if key == "escape" then

            lastState = state.current

            state.load(states.menu)

        elseif currentChoice == 0 and atStopState and
            (key == "left" or key == "right" or key == "up" or key == "down" or key == " ") then

            currentChoice = 1

        elseif key == " " then

            dialogue.click(0, 0)

        elseif currentText.type == "quiz" then

            if key == "left" or key == "right" then

                if currentChoice % 2 == 1 then

                    currentChoice = currentChoice + 1

                else

                    currentChoice = currentChoice - 1

                end

            elseif key == "up" or key == "down" then

                currentChoice = ((currentChoice + 1) % 4) + 1

            end

        elseif currentText.type == "choice" then

            if key == "up" then

                currentChoice = currentChoice - 1

                if currentChoice == 0 then

                    currentChoice = #currentText.choices

                end

            elseif key == "down" then

                currentChoice = ((currentChoice) % #currentText.choices) + 1

            end
        end

    elseif state.current == states.menu then

        if key == "escape" then

            state.set(states.play)

        end
    end

end
