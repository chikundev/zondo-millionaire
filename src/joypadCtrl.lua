if isOUYA then

    function love.joystickpressed(joy, button)

        if button == 1 then

            useKey(" ")

        elseif button == 12 then

            useKey("up")

        elseif button == 13 then

            useKey("down")

        elseif button == 14 then

            useKey("left")

        elseif button == 15 then

            useKey("right")

        end
    end

else

    function love.gamepadpressed(joy, button)

        if button == "a" then

            useKey(" ")

        elseif button:sub(1, 2) == "dp" then

            useKey(button:sub(3))

        elseif button == "back" or button == "guide" or button == "start" then

            useKey("escape")

        end
    end

end
