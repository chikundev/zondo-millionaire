-- Dialogue functions


dialogue = { }


quizBoxes = {
    {
        x = 32  ; y = 590 - 16,
        w = 576 ; h = 48
    },
    {
        x = 672 ; y = 590 - 16,
        w = 576 ; h = 48
    },
    {
        x = 32  ; y = 655 - 16,
        w = 576 ; h = 48
    },
    {
        x = 672 ; y = 655 - 16,
        w = 576 ; h = 48
    }
}


choiceBoxes = {
    {
        x = 440 ; y = 540 - 480,
        w = 400 ; h = 48
    },
    {
        x = 440 ; y = 540 - 384,
        w = 400 ; h = 48
    },
    {
        x = 440 ; y = 540 - 288,
        w = 400 ; h = 48
    },
    {
        x = 440 ; y = 540 - 192,
        w = 400 ; h = 48
    },
    {
        x = 440 ; y = 540 - 96,
        w = 400 ; h = 48
    }
}

currentChoice = 0


quizLetters = {
    "A. ", "B. ", "C. ", "D. "
}

textBack = 0


function dialogue.start(dia)

    -- Change scene's dialogue to new dialogue
    scene.dialogue = text[dia]

    -- Reset dialogue choice
    currentChoice = 0

    -- Restart dialogue loop
    scene.step = 1

    -- Text fades in, starting at zero
    scene.textFade = 1 ; scene.textFadeStep = 0

    local currentText = scene.dialogue[1]

    -- Current set functions

    -- Modify background
    if currentText.setBG == "none" then
        scene.bg = nil
    elseif currentText.setBG then
        scene.bg = currentText.setBG
    end

    -- Modify foreground
    if currentText.setFG == "none" then
        scene.fg = nil
    elseif currentText.setFG then
        scene.fg = currentText.setFG
    end

    -- Run code if exists
    if currentText.code then
        currentText.code()
    end

end


function dialogue.update(dt)


    -- Increment fading step=
    scene.textFadeStep =
        math.clamp(0, scene.textFadeStep + dt * scene.textFade * 2, 1)


    -- Determine current text segment
    local currentText = scene.dialogue[scene.step]

    -- Deal with textBack
    if currentText.speaker and currentText.speaker ~= "" then
        textBack = math.min(textBack + dt * 4, 1)
    else
        textBack = math.max(textBack - dt * 4, 0)
    end


    -- If type is text...
    if currentText.type == "text" then

        -- And we are fading out...
        if scene.textFadeStep == 0 then

            -- skip to next stage
            dialogue.skip()

        end

    -- If type is fade...
    elseif currentText.type == "fade" then

        -- And we've faded in...
        if scene.textFadeStep == 1 then

            -- skip to next stage
            dialogue.skip()

        end

    elseif currentText.type == "quiz" then

        -- And we've faded in...
        if scene.textFadeStep == 1 then

            -- If
            if math.overlapTable(quizBoxes, cursor) then

                nextCursor = "hand"

            end

        -- If we faded out...
        elseif scene.textFadeStep == 0 then

            -- skip to next stage
            dialogue.skip(currentText.choices[currentChoice])

        end


    elseif currentText.type == "choice" then

        -- And we've faded in...
        if scene.textFadeStep == 1 then

            local ansOffset = #choiceBoxes - #currentText.choices

            -- If
            for key, box in ipairs(choiceBoxes) do

                if math.overlap(box, cursor) and key > ansOffset then

                    nextCursor = "hand"

                end

            end

        -- If we faded out...
        elseif scene.textFadeStep == 0 then

            -- skip to next stage
            dialogue.skip(currentText.choices[currentChoice])

        end
    end


    if mouseControl and scene.textFadeStep == 1 then

        currentChoice = 0

        if currentText.type == "quiz" then

            for key, box in ipairs(quizBoxes) do

                if math.overlap(box, cursor) then

                    currentChoice = key

                end
            end
        elseif currentText.type == "choice" then

            local ansOffset = #choiceBoxes - #currentText.choices

            for key, box in ipairs(choiceBoxes) do

                if math.overlap(box, cursor) and key > ansOffset then

                    currentChoice = key - ansOffset

                end
            end
        end
    end

end


function dialogue.click(x, y)


    -- Get current text segment
    local currentText = scene.dialogue[scene.step]

    if scene.textFadeStep == 1 and scene.textFade == 1 then

        if (currentText.type == "quiz" or currentText.type == "choice") then

            if currentChoice > 0 then

                scene.textFade = -1

            end
        else
            scene.textFade = -1
        end

    elseif scene.textFade == 1 then

        scene.textFadeStep = 1

    end


end


function dialogue.skip(currentText)


    -- Find current text segment
    local currentText = currentText or scene.dialogue[scene.step]

    -- Resets choice
    currentChoice = 1


    -- Previous next functions

    -- Modify background
    if currentText.nextBG == "none" then
        scene.bg = nil
    elseif currentText.nextBG then
        scene.bg = currentText.nextBG
    end

    -- Modify foreground
    if currentText.nextFG == "none" then
        scene.fg = nil
    elseif currentText.nextFG then
        scene.fg = currentText.nextFG
    end

    -- Run code if exists
    if currentText.nextCode then
        currentText.nextCode()
    end

    -- Increase scene step
    if currentText.nextStep then
        scene.step = currentText.nextStep
    elseif currentText.nextDialogue then
        dialogue.start(currentText.nextDialogue)
        return true
    elseif currentText.nextState then
        state.load(states[currentText.nextState])
        return true
    else
        scene.step = scene.step + 1
    end


    -- Pick next text segment
    currentText = scene.dialogue[scene.step]


    -- Current set functions

    -- Modify background
    if currentText.setBG == "none" then
        scene.bg = nil
    elseif currentText.setBG then
        scene.bg = currentText.setBG
    end

    -- Modify foreground
    if currentText.setFG == "none" then
        scene.fg = nil
    elseif currentText.setFG then
        scene.fg = currentText.setFG
    end

    -- Run code if exists
    if currentText.code then
        currentText.code()
    end

    -- Reset fades
    scene.textFade = 1 ; scene.textFadeStep = 0

end


function dialogue.draw()


    -- Get current text segment
    local currentText = scene.dialogue[scene.step]


    -- Draw background
    if scene.bg then

        -- Set colour
        g.setColor(255, 255, 255)

        -- Draw background
        g.drawToSize(scene.bg, 0, 0, 1280)

    end

    -- If we're fading...
    if currentText.nextBG then

        if currentText.nextBG == "none" then

            g.setColor(0, 0, 0, 255 * scene.textFadeStep)

            g.rectangle('fill', 0, 0, 1280, 720)

        else

            g.setColor(255, 255, 255, 255 * scene.textFadeStep)

            g.drawToSize(currentText.nextBG, 0, 0, 1280)

        end
    end


    -- Draw foreground if not fading
    if scene.fg and not currentText.nextFG then

        g.drawToSize(scene.fg, 0, 0, 1280)

    elseif scene.fg and currentText.nextFG then

        if currentText.nextFG ~= "none" then

            g.setColor(255, 255, 255, 255 * scene.textFadeStep)

            g.drawToSize(currentText.nextFG, 0, 0, 1280)

        end

        g.setColor(255, 255, 255, 255 * (1 - scene.textFadeStep))

        g.drawToSize(scene.fg, 0, 0, 1280)

    elseif not scene.fg and currentText.nextFG then

        if currentText.nextFG ~= "none" then

            g.setColor(255, 255, 255, 255 * scene.textFadeStep)

            g.drawToSize(currentText.nextFG, 0, 0, 1280)

        end

    end

    g.setFont(fnt.regular)

    if textBack > 0 then

        g.rotate(math.rad(5 * textBack))

        g.setColor(61, 79, 94)

        g.rectangle('fill', 16 + 109 * textBack, 540 - 115 * textBack, 1248 - textBack * 78, 180 - 16)

        g.setColor(111, 140, 166)

        g.rotate(math.rad(-1 * textBack))

        g.rectangle('fill', 16 + 82 * textBack, 540 - 89 * textBack, 1248 - textBack * 48, 180 - 16)

        g.rotate(math.rad(-4 * textBack))

    end

    g.setColor(25, 31, 36)

    g.rectangle('fill', 16, 540, 1280 - 32, 180 - 16)

    local drawFunction = dialogue[currentText.type .. "Draw"]

    if drawFunction then drawFunction(currentText) end

end



function dialogue.textDraw(currentText)

    local headerStep = math.max(0, scene.textFadeStep - 0.25) / 0.75

    g.setColor(0, 0, 0, 255 * headerStep)

    g.print(currentText.speaker or "", 74, 502)

    g.setColor(255, 255, 255, 255 * headerStep)

    g.print(currentText.speaker or "", 72, 500)

    g.setColor(255, 255, 255, 255 * scene.textFadeStep)

    g.printf(currentText.text, 28, 558, 1176, 'left')


end


function dialogue.choiceDraw(currentText)

    local boxStep, headerStep =
        1 - math.max(0, 0.75 - scene.textFadeStep) / 0.75,
        math.max(0, scene.textFadeStep - 0.75) / 0.25

    local ansOffset = #choiceBoxes - #currentText.choices

    -- Draw answer insides
    for key, box in ipairs(choiceBoxes) do
        local yDiff = 540 - box.y
        local yPos = 540 - yDiff * boxStep
        if key > ansOffset then

            g.setColor(61, 79, 94, 255 * boxStep)   -- Reset colour

            if (key - ansOffset) == currentChoice then
                g.setColor(100, 120, 140, 255 * boxStep)
            end

            g.rectangle('fill', box.x, yPos,
                box.w, math.min(540 - yPos, box.h))

        end
    end

    g.setColor(255, 255, 255, 255 * headerStep)

    g.setFont(fnt.small)

    -- Draw text
    for key, box in ipairs(choiceBoxes) do
        if key > ansOffset then
            g.printf(currentText.choices[key - ansOffset].text,
                box.x, box.y + 11, box.w, 'center')
        end
    end

    g.setFont(fnt.regular)

    g.setColor(255, 255, 255, 255 * scene.textFadeStep)

    g.printf(currentText.text, 28, 558, 1176, 'justify')


end



function dialogue.quizDraw(currentText)


    -- Question we will deal with
    local question, boxFade, textFade =
        currentText.text,
        math.min(1, scene.textFadeStep * 2),
        math.max(0, (scene.textFadeStep - 0.5) * 2)

    -- Set font to very small
    g.setFont(fnt.smaller)

    -- Get height of question text
    local qVars = {g.getFont():getWrap(question, 1000)}
    local questionHeight = (qVars[2] * g.getFont():getHeight()) +
        (qVars[2] - 1) * g.getFont():getLineHeight()



    g.setColor(111, 140, 166, 255 * boxFade)   -- Reset colour

    local qY, qHeight =
        510 - questionHeight + (14 * math.max(0, (boxFade - 0.5) * 2)),
        questionHeight + 28

    -- Draw question inside
    g.rectangle('fill', 138, qY, 1004, qHeight)



    -- Draw answer insides
    for key, box in ipairs(quizBoxes) do

        g.setColor(61, 79, 94, 255 * boxFade)   -- Reset colour

        if key == currentChoice then
            g.setColor(100, 120, 140, 255 * boxFade)
        end

        g.rectangle('fill', box.x, box.y, box.w, box.h)
    end



    g.setColor(255, 255, 255, 255 * textFade)   -- Reset colour

    -- Draw text
    g.printf(question, 140, 556 - questionHeight - 16, 1000, 'center')

    g.setFont(fnt.small)

    -- Draw text
    for key, box in ipairs(quizBoxes) do
        g.printf(quizLetters[key] .. currentText.choices[key].text,
            box.x, box.y + 11, box.w, 'center')
    end

end
