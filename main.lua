-- chikun :: 2015
-- A tutorial covering the specific chikun LÖVE libraries

--[[
    Here we will load the chikun specific libaries
  ]]
require "lib/bindings"      -- Very important that you read this file
require "lib/maths"         -- Load maths functions
require "lib/misc"          -- Load miscellaneous functions

-- Are we on OUYA?
isOUYA = (s.getOS() == "Android")

--[[
    The recursive loading functions pretty much convert / to . and
    make file extensions void.
    eg. gfx/player.png becomes gfx.player,
        sfx/enemies/die becomes sfx.enemies.die
  ]]
require "lib/load/fonts"    -- Load fonts into fnt table [not recursive]
require "lib/load/gfx"      -- Load png graphics from gfx folder to gfx table
require "lib/load/snd"      -- Load ogg sounds from bgm and sfx to bgm and sfx
require "lib/load/states"   -- Load states from src/states to states table
require "lib/load/text"     -- Load texts from /text to text table

require "src/dialogue"      -- Load dialogue functions
require "src/joypadCtrl"    -- Load joypad controls functions
require "src/useKey"        -- Load useKey function
require "lib/stateManager"  -- Load state manager
require "lib/mapTool"       -- Load mapTool :: don't try reading this



-- Performed at game startup
function love.load()


    -- View positions
    getView()

    -- Set current state to play state
    state.load(states.splash)

    -- List of hardware cursors
    if s.getOS() ~= "Android" then
        cursors = {
            arrow = m.getSystemCursor('arrow'),
            hand = m.getSystemCursor('hand')
        }
    end

    menuButton = {
        x = 1156,
        y = 8,
        w = 96,
        h = 48
    }

    -- Mouse controlled?
    mouseControl = false

    mousePrevious = {
        x = m.getX(),
        y = m.getY()
    }

    -- Currently pressed keys
    keys = { }


end



-- Performed on game update
function love.update(dt)

    --[[
        Limit dt to 1/15 and induce lag if below 15 FPS.
        This is to save calculations.
      ]]
    dt = math.min(dt, 1/15)

    -- What the next hardware cursor will be
    nextCursor = "arrow"

    -- Shifts game view
    getView()

    -- Determines game scale
    scale = math.min(g.getWidth() / 1280, g.getHeight() / 720)

    -- Determine cursor position on screen
    cursor = {
        x = (m.getX() - view.x) / scale,
        y = (m.getY() - view.y) / scale,
        w = 1,
        h = 1
    }

    -- Update current state
    state.current:update(dt)

    -- Update mouse cursor
    if s.getOS() ~= "Android" then
        m.setCursor(cursors[nextCursor])
    end

    -- Mouse controlled?
    if m.getX() ~= mousePrevious.x or m.getY() ~= mousePrevious.y then
        mouseControl = true
    end

    mousePrevious = {
        x = m.getX(),
        y = m.getY()
    }


end



-- Performed on game draw
function love.draw()


    -- Set drawing colour to white [default]
    g.setColor(255, 255, 255)

    -- Set drawing scissor
    g.setScissor(view.x, view.y, 1280 * scale, 720 * scale)

    -- Offset due to view
    g.translate(view.x, view.y)

    -- Scale all drawing based on view
    g.scale(scale)

    -- Draw current state
    state.current:draw()

    if state.current == states.play then

        -- Draw menu button
        g.setColor(255, 255, 255)

        g.rectangle('fill', menuButton.x, menuButton.y,
            menuButton.w, menuButton.h)

        g.setColor(0, 0, 0)

        g.rectangle('fill', menuButton.x + 2, menuButton.y + 2,
            menuButton.w - 4, menuButton.h - 4)

        -- Draw menu text
        g.setColor(255, 255, 255)

        g.setFont(fnt.smaller)

        g.printf("Menu", menuButton.x, menuButton.y + menuButton.h / 2 -
            g.getFont():getHeight() / 2 + 2, menuButton.w, 'center')

    end

    g.setScissor()

    g.origin()


end



-- Performed on mouse press
function love.mousepressed(mb, x, y)

    if math.overlap(cursor, menuButton) and state.current == states.play then

        useKey("escape")

    elseif state.current.click then

        state.current:click(x, y)

    end

end



-- Performed on key press
function love.keypressed(key)

    -- Fullscreen toggle on F11
    if key == "f11" then

        w.setFullscreen(not w.getFullscreen(), 'desktop')

    end

    if key == "menu" then
        key = "escape"
    end

    if not keys[key] then

        useKey(key)

        keys[key] = true

    end
end


-- Performed on key release
function love.keyreleased(key)

    keys[key] = nil

end



function getView()


    local hRatio, vRatio =
        g.getWidth() / 16,
        g.getHeight() / 9

    view = {
        x = math.max((hRatio - vRatio) * 8,   0),
        y = math.max((vRatio - hRatio) * 4.5, 0)
    }


end
