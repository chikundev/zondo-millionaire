-- chikun :: 2014
-- Load required libraries


-- Load flags
require "src/flags"

-- Load shorthand bindings
require "lib/bindings"

-- Load maths functions
require "lib/maths"

-- Load miscellaneous functions
require "lib/misc"

-- Load resources
require "lib/load/fonts"    -- Load fonts       NOT recursive
require "lib/load/gfx"      -- Load graphics    recursive
require "lib/load/maps"     -- Load all maps    recursive
require "lib/load/snd"      -- Load sounds      recursive
require "lib/load/states"   -- Load states      recursive

-- Load scaling functions
require "lib/scaling"

-- Load state manager
require "lib/stateManager"

-- Load input manager, depending on OS
if s.getOS() == "Android" and false then
    require "lib/input/mobile"
else
    require "lib/input/desktop"
end

-- Load tile movement
require "src/tileMovement"
