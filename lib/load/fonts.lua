-- chikun :: 2014
-- Loads all fonts from the /fnt folder


function resizeFonts(scale)

    -- Can't do this recursively due to sizes
    fnt = {
        -- Smaller font
        smaller = love.graphics.newFont("fnt/novem___.ttf", 24 * scale),
        -- Small font
        small = love.graphics.newFont("fnt/novem___.ttf", 32 * scale),
        -- Regular font
        --regular = love.graphics.newFont("fnt/novem___.ttf", 40 * scale),
        regular = love.graphics.newImageFont("fnt/glyph.png",
            "abcdefghijklmnopqrstuvwxyz" ..
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ" ..
            "~`!@#$%^&*()-_+={[]}\\|:;" ..
            '"' .. "',<>.?/ " ..
            "1234567890"),
        -- Splash screen font
        splash  = love.graphics.newFont("fnt/exo2.otf", 144 * scale),
    }

    fnt.regular:setFilter("linear", "nearest", 1)
    fnt.small:setFilter("linear", "nearest", 1)
    fnt.smaller:setFilter("linear", "nearest", 1)

    fnt.regular:setLineHeight(1.2)

end

resizeFonts(1)
