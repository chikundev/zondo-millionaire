-- chikun :: 2014
-- Loads all text from the /text folder


-- Recursively checks a folder for texts
-- and adds any found to a text table
function checkFolder(dir, tab)

    local tab = ( tab or { } )

    -- Get a table of files / subdirs from dir
    local items = f.getDirectoryItems(dir)

    for key, val in ipairs(items) do

        if f.isFile(dir .. "/" .. val) then
            -- If item is a file, then load it into tab

            -- Remove ".lua" extension on file
            local name = val:gsub(".lua", "")

            -- Load state into table
            tab[name] = require(dir .. "/" .. name)

        else
            -- Else, run checkFolder on subdir

            -- Add new table onto tab
            tab[val] = { }

            -- Run checkFolder in this subdir
            checkFolder(dir .. "/" .. val, tab[val])

        end

    end

    return tab

end


-- Load the text folder into the text table
text = checkFolder("text")

-- Kills checkFolder
checkFolder = nil
